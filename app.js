import express from 'express';
import { portNumber } from './config';
import { basicRoutes } from './routes';
const app = express();

app.set('view engine', 'ejs');
app.use(basicRoutes);

app.listen(portNumber, () => {
     console.log(`Server running at ${portNumber}`);
});
