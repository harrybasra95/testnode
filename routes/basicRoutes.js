import express from 'express';
import { basicController } from '../controllers';
const router = express.Router({ mergeParams: true });

router.get('/', basicController.homepage);

export default router;
